using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
    public static bool TeleportCommand = false;
    public static bool PauseCommand = false;
    public static bool ChangeColorsCommand = false;
    public static bool SetColorMonochromatic = false;
    public static bool SetColorBoynton = false;
    public static bool SetColorKelly = false;
    public static bool AddZones = false;
    public static bool RemoveZones = false;
    public static bool Restart = false;

    public static bool ShuffleZones = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        TeleportCommand =  (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space));
        ChangeColorsCommand =  Input.GetKeyDown(KeyCode.W);
        PauseCommand = Input.GetKeyDown(KeyCode.K);
        SetColorMonochromatic = Input.GetKeyDown(KeyCode.Alpha1);
        SetColorBoynton = Input.GetKeyDown(KeyCode.Alpha2);
        SetColorKelly = Input.GetKeyDown(KeyCode.Alpha3);

        AddZones = Input.GetKeyDown(KeyCode.Plus) || Input.GetKeyDown(KeyCode.KeypadPlus) || Input.GetKeyDown(KeyCode.Equals);
        RemoveZones = Input.GetKeyDown(KeyCode.Minus) || Input.GetKeyDown(KeyCode.KeypadMinus) || Input.GetKeyDown(KeyCode.Underscore);

        Restart = Input.GetKeyDown(KeyCode.R);
        ShuffleZones = Input.GetKeyDown(KeyCode.Q);

    }
}
