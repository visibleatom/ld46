using UnityEngine;
using System.Collections;

public class Edge : MonoBehaviour
{
    private const float EPSILON_SQUARED = .01f * .01f;

    public bool IsVertical
    {
        get
        {
            return this.mIsVertical;
        }
        set
        {
            mIsVertical = value;
            UpdateScales();
        }
    }

    public bool Enabled
    {
        get
        {
            return this.mIsEnabled;
        }
        set
        {
            //Debug.Log("Edge::Enabled : " + value);
            mIsEnabled = value;
            renderer.enabled = value;
        }
    }

    public bool CloseEnough
    {
        get
        {
            return this.mCloseEnough;
        }
    }

    public Vector3 Speed;
    public ColorZone SafeZone;
    private bool mIsVertical;
    private bool mIsEnabled;
    private bool mTargetPositionDirty;
    private float mAcceptableDistanceSquared = .2f * .2f;
    protected bool mCloseEnough;
    private Vector3 mTargetPosition;
    protected Vector3 mSpeedModifier;

    void Awake()
    {
        mIsEnabled = false;
        mTargetPositionDirty = false;
        renderer.enabled = false;
        mSpeedModifier = new Vector3(1f,1f,1f);
    }

    // Use this for initialization
    void Start()
    {
        UpdateScales();
    }

    virtual protected float GetFakeTweener ()
    {
        return Time.deltaTime * 10;
    }
 
    // Update is called once per frame
    virtual public void Update()
    {
        Transform thisTransform = this.transform;

        float percentMove = GetFakeTweener();

        if (mTargetPositionDirty)
        {
            Vector3 currentPos = thisTransform.position;
            float sqrMag = (new Vector2(currentPos.x, currentPos.y) - new Vector2(mTargetPosition.x, mTargetPosition.y)).sqrMagnitude;
            if (sqrMag < EPSILON_SQUARED)
            {
                // We're close enough to stop
                mTargetPositionDirty = false;

                mCloseEnough = true;
            }
            else if (sqrMag < mAcceptableDistanceSquared)
            {
                mCloseEnough = true;
            }
            else
            {
                mCloseEnough = false;
            }
            Vector3 direction = (mTargetPosition - currentPos).normalized;
            thisTransform.Translate(new Vector3(
                direction.x * Speed.x * percentMove * mSpeedModifier.x,
                direction.y * Speed.y * percentMove * mSpeedModifier.y,
                direction.z * Speed.z * percentMove * mSpeedModifier.z));
        }

    }

    /// <summary>
    /// Updates the scales used for this Edge.
    /// </summary>
    void UpdateScales()
    {
        if (mIsVertical)
        {
            this.transform.localScale = new Vector3(.1f, 500f, .1f);
        }
        else
        {
            this.transform.localScale = new Vector3(500f, .1f, .1f);
        }
    }

    public void SetTargetY(float y)
    {
        //Debug.Log("Edge::SetTargetY y=" + y);
        mTargetPosition.y = y;
        mTargetPositionDirty = true;
    }

    public void SetTargetX(float x)
    {
        //Debug.Log("Edge::SetTargetX x=" + x);
        mTargetPosition.x = x;
        mTargetPositionDirty = true;
    }

    public void SetSpeedModifierX(float x)
    {
        mSpeedModifier.x = x;
    }

    public void SetSpeedModifierY(float y)
    {
        mSpeedModifier.y = y;
    }

    public void RemoveFromSafezone()
    {
        SafeZone = null;
        Enabled = false; //< NO! We will transition out if we want to.

    }
}
