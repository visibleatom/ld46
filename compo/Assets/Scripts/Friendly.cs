using UnityEngine;
using System.Collections;

public class Friendly : MonoBehaviour, Trackable
{
    private const float BASE_SCALE = .25f;
    public GameObject Body;
    public GameObject Head;
    public float Health = 1f;
    public ColorZone SafeZone;
    public Color DesiredColor;
    //public int ScaleMultiplier = 1; // The base scale is .25f;
    public bool CanBeHunted = false;
    public float TimeToEscape = 12f;

    enum State
    {
        UNUSED,
        TRANSITION_IN,
        ALIVE,
        DYING,
        DEAD,
        ESCAPED
    };

    private State mState;
    private float mTimeInState;
    private float mTransitionInTime = 1.5f;
    private float mTransitionDeathTime = 2.5f;


    // Use this for initialization
    void Start()
    {
        SetState(State.UNUSED);
    }
 
    // Update is called once per frame
    void Update()
    {
        mTimeInState += Time.deltaTime;
        //Transform thisTransform = this.transform;
        switch (mState)
        {
        case State.TRANSITION_IN:

            UpdatePosition();


            if (mTimeInState < mTransitionInTime)
            {
                Head.renderer.material.color = Body.renderer.material.color = Color.Lerp(Body.renderer.material.color, DesiredColor, mTimeInState / mTransitionInTime);
            }
            else
            {
                SetState(State.ALIVE);
            }
            break;

        case State.ALIVE:
            UpdatePosition();
            if (Health <= 0f)
            {
                SetState(State.DYING);
            }
            else
            {
                if (mTimeInState > TimeToEscape)
                {
                    SetState(State.ESCAPED);
                }

            }
            break;
        case State.DYING:
            UpdatePosition();
            if (mTimeInState < mTransitionDeathTime)
            {
                Head.renderer.material.color = Body.renderer.material.color = Color.Lerp(DesiredColor, Color.black, mTimeInState / mTransitionDeathTime);
            }
            else
            {
                SetState(State.DEAD);
            }
            break;
        }
    }

    void UpdatePosition()
    {
        Transform thisTransform = this.transform;

        thisTransform.localScale = new Vector3(BASE_SCALE * Health, BASE_SCALE * Health, BASE_SCALE * Health);
        Vector3 newPos = SafeZone.transform.position;
        newPos.z = -5;
        thisTransform.position = newPos;
    }

    void SetState(State newState)
    {
        //Debug.Log("Friendly::SetState " + newState);
        mState = newState;
        mTimeInState = 0f;
        switch (newState)
        {
        case State.TRANSITION_IN:
            CanBeHunted = false;
            break;
        case State.ALIVE:
            CanBeHunted = true;
            Head.renderer.material.color = Body.renderer.material.color = DesiredColor;
            // rotate 0
            break;
        case State.DYING:
        case State.DEAD:
            CanBeHunted = false;
            Head.renderer.material.color = Body.renderer.material.color = Color.black;
            // rotate 90 this.transform.localEulerAngles.z = 90;
            break;
        case State.ESCAPED:
        case State.UNUSED:
            CanBeHunted = false;
            if (SafeZone != null)
            {
                SafeZone.Friend = null;
                SafeZone = null;
            }
            // For now, unused and escaped are the same thing.
            Head.renderer.material.color = Body.renderer.material.color = Color.clear;
            break;
        }
    }


    #region Trackable implementation
    public bool VisibleInZone()
    {
        return CanBeHunted;
    }

    public float GetPositionX()
    {
        return this.transform.position.x;
    }

    public float GetPositionY()
    {
        return this.transform.position.y;
    }
    #endregion

    public bool HasEscaped()
    {
        return mState == State.ESCAPED;
    }

    public bool HasDied()
    {
        return mState == State.DEAD;
    }

    public void ResetState()
    {
        SetState(State.UNUSED);
    }

    public bool IsActive()
    {
        return mState != State.UNUSED;
    }

    public void SpawnInZone(ColorZone zone)
    {
        SafeZone = zone;
        SafeZone.Friend = this;
        SetState(State.TRANSITION_IN);
    }

    #region Trackable implementation
    public void Kill()
    {
        SetState(State.DYING);
    }
    #endregion
}
