using UnityEngine;
using System.Collections;

// This guy controls:
// * Safe Zone colors
// * Safe Zone movement
// * Grid settings
// * Sweeper Settings
// * 'Global' place with references to rest of level.
public class LevelController : MonoBehaviour
{
    private const int MAX_NUM_FRIENDS = 10;
    private const int MAX_NUM_EDGES = 80;
    private const int MIN_NUM_ZONES = 5;
    private const int MAX_NUM_ZONES = MAX_NUM_EDGES / 4; //< our current design constraints make this a hard limit.
    private const int MAX_NUM_SAFE_COLORS = 9;
    private const int MAX_NUM_ENEMIES = 3;
    private const float SCREEN_MIN_X = -12f;
    private const float SCREEN_MAX_X = 12f;
    private const float SCREEN_MIN_Y = -10f;
    private const float SCREEN_MAX_Y = 10f;
    public PlayerController Player;

    // prefabs (for Instantiate)
    public Edge EdgePrefab;
    public Sweeper EnemyEdgePrefab;
    public Friendly FriendlyPrefab;
    public ColorZone SafeZoneAreaPrefab;
    public Camera MainCamera;
    public InstructionController Instructions;


    // Target zones. This is where the player can end up teleporting to.
    private ColorZone mCurrentTargetZone;
    private ColorZone mPreviousTargetZone;
    private ColorZone mNextTargetZone;
    private Edge[] mEdges;
    private ColorZone[] mZones;
    private bool mIsPaused = false;
    private Color[] mAvailableColors;
    private int mMaxColors;
    private float mTimeUntilZoneChange = 15f;
    private float mMinZoneSpawnTime = 6f;
    private float mMaxZoneSpawnTime = 12f;
    private int mChanceForDoubleSpawn = 50;
    private float mTimeUntilFriendSpawn = 3f;
    private float mMinFriendSpawnTime = 10f;
    private float mMaxFriendSpawnTime = 10f;
    private Sweeper[] mEnemies;
    private Friendly[] mFriends;
    private int mNumMaxZones;
    private int mEscapedFriendlies = 0;
    private int mDeadFriendlies = 0;

    // Use this for initialization
    void Start()
    {
        // Create max edges, and max safezones. We'll reuse these as the game progresses.
        mEdges = new Edge[MAX_NUM_EDGES];

        for (int i = 0; i < MAX_NUM_EDGES; i++)
        {
            mEdges[i] = (Edge)Instantiate(EdgePrefab, new Vector3(1f, 0f, 1f), Quaternion.identity);
            mEdges[i].Speed.x = Random.Range(0.5f, 1f);
            mEdges[i].Speed.y = Random.Range(0.5f, 1f);
        }

        mZones = new ColorZone[MAX_NUM_ZONES];
        for (int i = 0; i < MAX_NUM_ZONES; i++)
        {
            mZones[i] = (ColorZone)Instantiate(SafeZoneAreaPrefab);
            mZones[i].gameObject.name = "SafeZoneIndex" + i;
            mZones[i].IsEnabled = false;
        }

        mAvailableColors = new Color[MAX_NUM_SAFE_COLORS];
        SetBoyntonColors();

        mEnemies = new Sweeper[MAX_NUM_ENEMIES];
        for (int i = 0; i < MAX_NUM_ENEMIES; i++)
        {
            mEnemies[i] = (Sweeper)Instantiate(EnemyEdgePrefab);
            mEnemies[i].AddTracked(Player, true);
            mEnemies[i].AddDimensions(SCREEN_MIN_X, SCREEN_MIN_Y, SCREEN_MAX_X - SCREEN_MIN_X, SCREEN_MAX_Y - SCREEN_MIN_Y);
            mEnemies[i].ChaseSpeedModifier = 1f;
            mEnemies[i].NoAlarmSpeedModifier = .5f;
            mEnemies[i].Speed.x = Random.Range(.05f, .09f);
            mEnemies[i].Speed.y = Random.Range(.05f, .09f);
        }

        mFriends = new Friendly[MAX_NUM_FRIENDS];
        for (int i =  0; i < MAX_NUM_FRIENDS; i++)
        {
            mFriends[i] = (Friendly)Instantiate(FriendlyPrefab);
        }

        float x = 3f;
        float y = -1f;
        float width = 2f;
        float height = 2f;

        mNumMaxZones = MIN_NUM_ZONES;

        // Enable 4 of them, and link them to a safezone.
        ColorZone safe = CreateSafeZone(x, y, width, height, GetAvailableColor());
        safe.SetUsed();
        for (int i = 0; i < mNumMaxZones - 1; i++)
        {
            CreateSafeZone(x, y, width, height, GetAvailableColor()).SetUsed();
        }

        SetTargetZone(safe);

        //Player.SetZone(safe);


    }
    
    // Update is called once per frame
    void Update()
    {
        if (Player.IsDead())
        {
            MainCamera.backgroundColor = Color.black;
            Instructions.DisplayTimeAlive(Player.mTimeAlive);
            Instructions.Show(true);
        }
        else if (AnyActiveEnemy())
        {
            MainCamera.backgroundColor = new Color(.9f, .9f, .9f);
        }
        else
        {
            MainCamera.backgroundColor = Color.white;
            if (Instructions != null)
            {
                Instructions.Show(Player.HasZone() == false);
            }

        }

        // Spawn a sweeper? Is it POSSIBLE for the player to escape it?
        // Check whether the angle of the current sweeper can intersect both safezones
        // (the previous, not-safe-anymore safe zone, and the current, will-be-safe safezone)
        // If it will intersect, then it's not reasonable to expect the player to escape it.
        // This implies that either the safezones DO NOT MOVE or else that I need to come up
        // with temporal collision detection. OK.  SECOND DAY STRETCH GOAL
        // TODO - STRETCH GOAL - temporal collision detection. Given the current zones' direction
        // and speed, will the sweeper intersect with BOTH of them at any given point?
        // If not, still mark the times that it WILL intersect with either as a 'danger time' for that
        // safezone. Make sure that there are no overlapping danger times between both safezones. If there
        // are, then we cannot spawn a sweeper.
        // This is assuming a procedural enemy spawning. If I end up hardcoding the level information, then
        // it becomes a matter of trial and error.
        if (InputController.PauseCommand == true)
        {
            mIsPaused = !mIsPaused;

            if (mIsPaused)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }

            for (int i = 0; i < MAX_NUM_ENEMIES; i++)
            {
                mEnemies[i].IsPaused = mIsPaused;
            }

        }
        else
        {
            if (InputController.Restart)
            {
                Application.LoadLevel("level");
            }

            if (InputController.SetColorMonochromatic == true)
            {
                SetMonochromatic();
                Recolor();
            }
            else if (InputController.SetColorBoynton == true)
            {
                SetBoyntonColors();
                Recolor();
            }
            else if (InputController.SetColorKelly == true)
            {
                SetKellysMaxContrastColors();
                Recolor();
            }

            if (InputController.ChangeColorsCommand == true)
            {
                ForcePlayerChange();
            }

            if (InputController.AddZones == true)
            {
                if (mNumMaxZones < MAX_NUM_ZONES)
                {
                    mNumMaxZones++;
                }
                int numActive = 0;
                for (int i = 0; i < MAX_NUM_ZONES; i++)
                {
                    numActive += (mZones[i].IsEnabled ? 1 : 0);
                }
                while (numActive < mNumMaxZones)
                {
                    SpawnRandomZone();
                    numActive++;
                }

            }
            else if (InputController.RemoveZones == true)
            {
                if (mNumMaxZones > MIN_NUM_ZONES)
                {
                    mNumMaxZones--;
                }
                int numActive = 0;
                for (int i = 0; i < MAX_NUM_ZONES; i++)
                {
                    numActive += (mZones[i].IsEnabled ? 1 : 0);
                }
                while (numActive > mNumMaxZones)
                {
                    GrabNonTargetZone(0).RemoveZone();
                    numActive--;
                }

            }

            if (InputController.ShuffleZones)
            {
                ShuffleNonTargets();
            }
        }

        if (!mIsPaused)
        {
            if (mNextTargetZone != null && mNextTargetZone.CanJumpInto())
            {
                SetTargetZone(mNextTargetZone);
            }


            // TODO - this should come from some other driver. A level text file, for instance.

            if (mTimeUntilZoneChange > 0f)
            {
                mTimeUntilZoneChange -= Time.deltaTime;
                if (mTimeUntilZoneChange <= 0f)
                {
                    ShuffleNonTargets();

                    //SpawnRandomZone();

                    //if (Random.Range(0, 100) < mChanceForDoubleSpawn)
                    //{
                    //    SpawnRandomZone();
                    //}

                    mTimeUntilZoneChange = Random.Range(mMinZoneSpawnTime, mMaxZoneSpawnTime);
                }
            }

            // We only bother with sweepers once the player has spawned.
            if (Player.HasZone())
            {
    
    
                if (mTimeUntilFriendSpawn > 0f)
                {
                    mTimeUntilFriendSpawn -= Time.deltaTime;
    
                    if (mTimeUntilFriendSpawn <= 0f)
                    {
                        SpawnSweeper(Player.transform.position);
                        //SpawnFriends();
                        if (Random.Range(0, 100) < mChanceForDoubleSpawn)
                        {
                            // Nope. 1's hard enough to track
                            //SpawnFriends();
                        }
    
                        mTimeUntilFriendSpawn = Random.Range(mMinFriendSpawnTime, mMaxFriendSpawnTime);
                    }
                }
    
                int numSweeps = 0;
                for (int i = 0; i < MAX_NUM_ENEMIES; i++)
                {
                    if (mEnemies[i].IsActive())
                    {
                        numSweeps++;
                        break;
                    }
                }
    
                int numVisibleFriendles = 0;
                Vector3 sweepGoal = Vector3.zero;
                for (int i = 0; i < MAX_NUM_FRIENDS; i++)
                {
                    if (mFriends[i].CanBeHunted)
                    {
                        numVisibleFriendles++;
                        sweepGoal = mFriends[i].transform.position;
                    }
                }
    
                int desiredSweeps = 0;
                if (Player.VisibleInZone())
                {
                    sweepGoal = Player.transform.position;
                    desiredSweeps++;
                }
    
                if (numVisibleFriendles > 2)
                {
                    desiredSweeps++;
                }
                else if (numVisibleFriendles > 0 && desiredSweeps == 0)
                {
                    desiredSweeps++;
                }
    
                while (numSweeps < desiredSweeps)
                {
                    SpawnSweeper(sweepGoal);
                    numSweeps++;
                }
            }

            for (int i = 0; i < MAX_NUM_FRIENDS; i++)
            {
                if (mFriends[i].HasEscaped())
                {
                    mEscapedFriendlies++;
                    mFriends[i].ResetState();
                }
                if (mFriends[i].HasDied())
                {
                    mDeadFriendlies++;
                    mFriends[i].ResetState();
                }

                if (mFriends[i].CanBeHunted)
                {
                }
            }

            // Check collisions between sweeper and player


        }
    }

    void SpawnSweeper(Vector3 targetPos)
    {
        //Debug.Log("SpawnSweeper against : " + targetPos.ToString());
        bool verticalSweep;
        bool leftToRight;
        bool topToBottom;

        Sweeper enemy = null;
        // find first available sweeper
        for (int i = 0; i < MAX_NUM_ENEMIES; i++)
        {
            if (!mEnemies[i].IsActive())
            {
                enemy = mEnemies[i];
                break;
            }
        }

        // early return
        if (enemy == null)
        {
            //Debug.Log("No free enemies");
            return;
        }

        enemy.GetSweepPreferences(targetPos, out leftToRight, out topToBottom, out verticalSweep);

        int numVertical = 0;
        int numHorizontal = 0;

        // However, we have to take into consideration any OTHER active sweepers.
        for (int i = 0; i < MAX_NUM_ENEMIES; i++)
        {
            if (mEnemies[i].IsActive())
            {
                numVertical += (mEnemies[i].IsVertical ? 1 : 0);
                numHorizontal += (mEnemies[i].IsVertical ? 0 : 1);
            }
        }

        if (numVertical != numHorizontal)
        {
            verticalSweep = (numHorizontal > numVertical);
        }

        Vector3 sweepOrigin;
        Vector3 dest;
        if (verticalSweep)
        {
            if (topToBottom)
            {
                sweepOrigin = new Vector3(0, SCREEN_MAX_Y + 1, 0);
                dest = new Vector3(0, SCREEN_MIN_Y - 1, 0);
     
            }
            else
            {
                sweepOrigin = new Vector3(0, SCREEN_MIN_Y - 1, 0);
                dest = new Vector3(0, SCREEN_MAX_Y + 1, 0);
            }
        }
        else
        {
            if (leftToRight)
            {
                sweepOrigin = new Vector3(SCREEN_MIN_X - 1, 0, 0);
                dest = new Vector3(SCREEN_MAX_X + 1, 0, 0);
            }
            else
            {
                sweepOrigin = new Vector3(SCREEN_MAX_X + 1, 0, 0);
                dest = new Vector3(SCREEN_MIN_X - 1, 0, 0);
            }
        }

     
        enemy.BeginSweep(sweepOrigin, dest);
    }
    
    void SetKellysMaxContrastColors()
    {
        // I grabbed these colors from
        // http://stackoverflow.com/questions/470690/how-to-automatically-generate-n-distinct-colors
        mAvailableColors[0] = new Color(1, (179f / 255f), 0);               // Vivid Yellow
        mAvailableColors[1] = new Color(.5f, (62f / 255f), (117f / 255f));     // Strong Purple
        mAvailableColors[2] = new Color(1f, (104f / 255f), 0);               // Vivid Orange
        mAvailableColors[3] = new Color((166f / 255f), (189f / 255f), (215f / 255f));     // Very Light Blue
        mAvailableColors[4] = new Color((193f / 255f), (0f / 255f), (32f / 255f));     // Vivid Red
        mAvailableColors[5] = new Color((206f / 255f), (162f / 255f), (98f / 255f));     // Grayish Yellow
        mAvailableColors[6] = new Color((129f / 255f), (112f / 255f), (102f / 255f));    // Medium Gray
        mMaxColors = 7;

        //UIntToColor(FFB300), //Vivid Yellow
        //UIntToColor(803E75), //Strong Purple
        //UIntToColor(FF6800), //Vivid Orange
        //UIntToColor(A6BDD7), //Very Light Blue
        //UIntToColor(C10020), //Vivid Red
        //UIntToColor(CEA262), //Grayish Yellow
        //UIntToColor(817066), //Medium Gray
    }

    void SetBoyntonColors()
    {
        // I grabbed these colors from
        // http://stackoverflow.com/questions/470690/how-to-automatically-generate-n-distinct-colors
        // This is the Boynton Optimized color set
        mAvailableColors[0] = new Color(0, 0, 1f);      //Blue
        mAvailableColors[1] = new Color(1f, 0, 0);      //Red
        mAvailableColors[2] = new Color(0, 1f, 0);      //Green
        mAvailableColors[3] = new Color(1f, 1f, 0);    //Yellow
        mAvailableColors[4] = new Color(1f, 0, 1f);    //Magenta
        mAvailableColors[5] = new Color(1f, .5f, .5f);  //Pink
        mAvailableColors[6] = new Color(.5f, .5f, .5f);  //Gray
        mAvailableColors[7] = new Color(.5f, 0, 0);      //Brown
        mAvailableColors[8] = new Color(1, .5f, 0);    //Orange

        mMaxColors = MAX_NUM_SAFE_COLORS;
    }

    void SetMonochromatic()
    {
        //mAvailableColors[0] = new Color(0, 0, .01f);
        mAvailableColors[0] = new Color(.25f, .25f, .25f);
        mAvailableColors[1] = new Color(.5f, .5f, .5f);
        mAvailableColors[2] = new Color(.9f, .9f, .9f);
        mMaxColors = 3;
    }

    ColorZone GrabNonTargetZone(int unusedPriorityWeight)
    {
        ColorZone safe = null;


        bool tryNotUsedFirst = Random.Range(0, 99) < unusedPriorityWeight;
        if (tryNotUsedFirst)
        {
            for (int i = 0; i < mNumMaxZones; i++)
            {
                if (mZones[i].IsEnabled == false)
                {
                    safe = mZones[i];
                    break;
                }
            }
        }

        int randomOffset = Random.Range(0, mNumMaxZones);
        int realIdx;
        for (int i = 0; i < mNumMaxZones && safe == null; i++)
        {
            realIdx = (randomOffset + i) % mNumMaxZones;
            safe = mZones[realIdx];

            if (!safe.IsEnabled || safe == mCurrentTargetZone || safe == mPreviousTargetZone || safe == mNextTargetZone || safe.HasPlayer())
            {
                // Nope, not good enough for our baby.
                safe = null;
            }

            // Also, don't grab this if it has friends spawned on it
            // TODO TODO TODO

        }

        if (safe == null)
        {
            throw new System.Exception("Could not find an ENABLED, non-target zone to reuse");
        }

        if (!safe.IsEnabled)
        {
            // For this test, we will find 4 edges that are NOT a part of a safezone yet.
            // If they are NOT in the view, they can be either vertical or not (it won't matter, after all).
            // If they ARE in the view, then we can only have up to 2 IsVertical

            int edgeIdx = 0;
            Edge [] tmpEdges = new Edge[MAX_NUM_EDGES];
            int numVertical = 0;
            int numHorizontal = 0;
            
            for (int i = 0; i < MAX_NUM_EDGES && edgeIdx < 4; i++)
            {
                if (mEdges[i].SafeZone == null)
                {
                    if (!mEdges[i].Enabled)
                    {
                        tmpEdges[edgeIdx++] = mEdges[i];
                    }
                    else if (mEdges[i].IsVertical && numVertical < 2)
                    {
                        numVertical++;
                        tmpEdges[edgeIdx++] = mEdges[i];
                        safe.AddEdge(mEdges[i]);
                    }
                    else if (!mEdges[i].IsVertical && numHorizontal < 2)
                    {
                        numHorizontal++;
                        tmpEdges[edgeIdx++] = mEdges[i];
                        safe.AddEdge(mEdges[i]);
                    }
                }
            }
    
            if (edgeIdx == 4 || safe.IsEnabled)
            {
                // I can proceed. Now, create a safezone, give it the desired constraints, join the edges to it, and
                // go to town.
                for (int i = 0; i < edgeIdx; i++)
                {
                    if (!tmpEdges[i].Enabled && tmpEdges[i].SafeZone == null)
                    {
                        safe.AddEdge(tmpEdges[i]);
                    }
                }
            }
            else
            {
                throw new System.Exception();
                // Hm. should we be worried?
            }
        }

        return safe;
    }

    /// <summary>
    /// Spawns the random zone.
    /// </summary>

    void SpawnRandomZone()
    {
        ColorZone newZone = GrabNonTargetZone(100);
        Rect dims = GetRandomZoneDimensions();
        PlaceZone(newZone, dims.x, dims.y, dims.width, dims.height);
        int newIdx = GetAvailableColor();
        newZone.SetColor(mAvailableColors[newIdx], newIdx);
        newZone.SetUsed();
    }

    void ShuffleNonTargets()
    {
        ColorZone safe;
        for (int i = 0; i < MAX_NUM_ZONES; i++)
        {
            safe = mZones[i];
            if (safe.IsEnabled && Player.GetCurrentZone() != safe && Player.GetTargetZone() != safe)
            {
                Rect dims = GetRandomZoneDimensions();
                PlaceZone(safe, dims.x, dims.y, dims.width, dims.height);
                int newIdx = GetAvailableColor();
                safe.SetColor(mAvailableColors[newIdx], newIdx);
                safe.SetUsed();
            }
        }
    }

    void ForcePlayerChange()
    {
        //mNextTargetZone = null;//mCurrentTargetZone = null;
        mNextTargetZone = GrabNonTargetZone(0);

        //Rect dims = GetRandomZoneDimensions();
        //PlaceZone(mNextTargetZone, dims.x, dims.y, dims.width, dims.height);

        // TODO - depending on sweeper positions, we should change the location of the next target zone.
     
        int newIdx = GetAvailableColor();
        mNextTargetZone.SetColor(mAvailableColors[newIdx], newIdx);
        mNextTargetZone.SetUsed();
        Player.SetTargetZone(mNextTargetZone);
    }

    Rect GetRandomZoneDimensions()
    {
        Rect dims = new Rect();
        dims.x = Random.Range(SCREEN_MIN_X, SCREEN_MAX_X);
        dims.y = Random.Range(SCREEN_MIN_Y, SCREEN_MAX_Y - 2f);
        dims.width = Random.Range(1.5f, 2f);
        dims.height = Random.Range(1.5f, 2f);
        return dims;
    }

    private int GetAvailableColor()
    {
        // If there's a next target, then we cannot pick this color.
        int idx = Random.Range(0, mMaxColors);
        int noIdx = -1;
        if (mNextTargetZone != null)
        {
            noIdx = mNextTargetZone.GetSafeColorIndex();
        }

        if (mCurrentTargetZone != null && mCurrentTargetZone.HasPlayer() == false)
        {
            noIdx = mCurrentTargetZone.GetSafeColorIndex();
        }
        while (idx == noIdx)
        {
            idx = Random.Range(0, mMaxColors);
        }

        return idx;
    }

    void Recolor()
    {
        // Change all the zones' colors, first.
        // Then, force-change the player's color.
        for (int i = 0; i < mNumMaxZones; i++)
        {
            int newIdx = GetAvailableColor();

            mZones[i].SetColor(mAvailableColors[newIdx], newIdx);
        }
        ForcePlayerChange();

    }

    /// <summary>
    /// Sets the target zone.
    /// </summary>
    /// <param name='safe'>
    /// Safe.
    /// </param>

    void SetTargetZone(ColorZone safe)
    {
        if (mCurrentTargetZone != null)
        {
            //mCurrentTargetZone.TransitionToUnsafe(3f);
        }

        mPreviousTargetZone = mCurrentTargetZone;
        mCurrentTargetZone = safe;
        mNextTargetZone = null;

        // Make sure NO OTHER SAFEZONES have the same color index as the target

        int targetIndex = mCurrentTargetZone.GetSafeColorIndex();
        for (int i = 0; i <mNumMaxZones; i++)
        {
            if (mZones[i].IsEnabled && mZones[i] != mCurrentTargetZone)
            {
                while (mZones[i].GetSafeColorIndex() == targetIndex)
                {
                    int newIdx = GetAvailableColor();
                    // yay bogo
                    mZones[i].SetColor(mAvailableColors[newIdx], newIdx);
                }
            }
        }

        // TODO - alternative: Change the color of THIS SAFEZONE to something that's unique.
        // Either way.

        Player.SetColor(mAvailableColors[targetIndex], targetIndex);

    }

    public ColorZone GetValidJumpZone()
    {
        return mCurrentTargetZone;
    }

    bool AnyActiveEnemy()
    {
        for (int i = 0; i < MAX_NUM_ENEMIES; i++)
        {
            if (mEnemies[i].IsActive())
            {
                return true;
            }
        }
        return false;
    }
    
    /// <summary>
    /// Places the zone.
    /// </summary>
    /// <param name='safe'>
    /// Safe.
    /// </param>
    /// <param name='x'>
    /// X.
    /// </param>
    /// <param name='y'>
    /// Y.
    /// </param>
    /// <param name='width'>
    /// Width.
    /// </param>
    /// <param name='height'>
    /// Height.
    /// </param>
    void PlaceZone(ColorZone safe, float x, float y, float width, float height)
    {

        if (safe.HasPlayer())
        {
            Debug.Log(" NO NO NO NO NO NO NO");
        }
        // HOLY CRAP I'M TIRED.
        // We are going to BOGO-check this
    
        // Dimensions we're asking for
        Rect dims = new Rect(x, y, width, height);
        bool collisions = true;
        int triedEnough = 0;
        const int MAX_TRIES = 1000;
        const float PADDING = .1f;
        while (collisions && triedEnough++ < MAX_TRIES)
        {
            collisions = false;
    
            Rect zoneDim;
            //Debug.Log("Dimensions Attempt for : "+ safe.gameObject.name + " :: " + dims.ToString());
            // I know that this is going through the same list we just went. I don't want to worry about optimizations.
            // I think this loop is fairly inexpensive.
            for (int i = 0; i < mNumMaxZones; i++)
            {
                if (mZones[i].IsEnabled)
                {
                    // Check against the coordinates to make sure that we don't overlap any dimentions. This is
                    // more of a heuristic, so we don't get ugly lines over any of the safe zones.
                    // Actually... Maybe better if we have a 'get out of the way' logic.
    
                    // Problem is that in this case, what happens is that ... ok I'm designing a spring system at this
                    // point. Since every active safe zone will need to move every other safe zone out of the way if they move
                    // I think it's easier for me to just find an empty spot.

                    // Simplest (and more constrictive method) is to prevent the safe zones from overlapping in ANY axis.
                    zoneDim = mZones[i].GetDimensions();
                    zoneDim.x -= PADDING;
                    zoneDim.y -= PADDING;
                    zoneDim.height += PADDING * 2;
                    zoneDim.width += PADDING * 2;
    
                    //Debug.Log("Dimensions for : "+ mSafeZones[i].gameObject.name + " :: " + zoneDim.ToString());
                    if (zoneDim.Contains(new Vector2(dims.x, dims.y)))
                    {
                        // It contains the top left.
                        collisions = true;
                    }
    
                    if (zoneDim.Contains(new Vector2(dims.xMax, dims.y)))
                    {
                        // It contains the top right. Lower the whole thing.
                        collisions = true;
                    }
    
                    if (zoneDim.Contains(new Vector2(dims.x, dims.yMax)))
                    {
                        // Lower left
                        collisions = true;
                    }
    
                    if (zoneDim.Contains(new Vector2(dims.xMax, dims.yMax)))
                    {
                        // Lower right
                        collisions = true;
                    }
                    // A more generous heuristic is to also take the edges making up those other safezones.
                }
            }
            if (collisions)
            {
                dims = GetRandomZoneDimensions();
            }
    
        }
        if (triedEnough >= MAX_TRIES)
        {
            Debug.Log("tried enough? : " + triedEnough);
        }
    
        // For now, make it a fixed dimension. No soft limits
        safe.SetTargetDimension(dims.x, dims.y, dims.width, dims.height);
    
    }

    ColorZone CreateSafeZone(float x, float y, float width, float height, int newColorIndex)
    {
        // Let's define how safezones and edges interact.
        // While a safezone is connected to edges AND THE SAFEZONE IS ACTIVE,
        // the edges CANNOT disconnect from each other. They form a closed area and
        // must remain so.
        // Redundant/safety: when the safezone deactivates, it should break any connections it has with
        // the edges.
        // A safezone might have certain constraints, and if that's the case, the edges will move into
        // position in order to satisfy those constraints.  So, if we want a minEdge for the safezone
        // and a minArea, we move the existing edges closer/farther away from each other until we hit that
        // constraint. Same deal if we want to center the safezone somewhere.

        // Edges without a connected safezone are free to wander about, connected to other edges
        // if they want.

        // Edges can connect to up to 2 other edges, as long as they have a different IsVertical than the
        // edges they are connecting two.  We might not have perfectly perpendicular edges in the future,
        // but I don't want to worry about that right now.
        // If they connect to an edge, they will STOP THERE. Otherwise, they will continue to infinity (500 units away)
        // Edges will only end mid-screen during transitions in/out of the screen.

        // NOTE: I realize that it's artistically possible for one edge to serve 2 different safe zones. I simply
        // want safezones to be separate enough, distance-wise, that it makes this case not worth worrying about.
        // Also, this way if 2 safezone have conflicting constraints, I don't need to have an edge-fight.

        // OK, I thought of a slightly harder problem to deal with.
        //
        //


        ColorZone safe = null;

        // Give priority to grabbing an unused safe zone (weight of 100). Otherwise,
        // re-use an existing safe zone, as long as it's none of our current target zones
        // we should be fine.
        safe = GrabNonTargetZone(100);

        if (safe != null)
        {
            PlaceZone(safe, x, y, width, height);
            safe.SetColor(mAvailableColors[newColorIndex], newColorIndex);
            safe.IsEnabled = true;
        }
        else
        {
            throw new System.Exception();
        }
        return safe;
    }

    void SpawnFriends()
    {
        Debug.Log("SpawnFriends");
        // Do we have any available friend/spawn pools?
        Friendly unusedFriendly = GetUnusedFriendly();
        if (unusedFriendly != null)
        {
            int numFreeZones = 0;
            for (int i = 0; i < mNumMaxZones; i++)
            {
                if (mZones[i].IsEnabled && mZones[i].Friend == null && !mZones[i].HasPlayer())
                {
                    numFreeZones++;
                }
            }

            if (numFreeZones > 0)
            {
    
                int freeIdx = Random.Range(0, numFreeZones - 1);
    
                ColorZone newZone = null;
    
                for (int i = 0; i < mNumMaxZones; i++)
                {
                    if (mZones[i].IsEnabled && mZones[i].Friend == null && !mZones[i].HasPlayer())
                    {
                        if (freeIdx == 0)
                        {
                            newZone = mZones[i];
                            break;
                        }
                        freeIdx--;
                    }
                }

                if (newZone != null)
                {
                    int colorIdx = GetAvailableColor();
                    while (colorIdx == newZone.GetSafeColorIndex())
                    {
                        colorIdx = GetAvailableColor();
                    }
                    unusedFriendly.DesiredColor = Color.white;//mAvailableColors[colorIdx];
                    unusedFriendly.SpawnInZone(newZone);

                    for (int i = 0; i < MAX_NUM_ENEMIES; i++)
                    {
                        mEnemies[i].AddTracked(unusedFriendly, false);
                    }
                }
                else
                {
                    Debug.Log("newZone is null. something wrong in selection process");
                }
            }
        }
    }

    Friendly GetUnusedFriendly()
    {
        for (int i = 0; i < MAX_NUM_FRIENDS; i++)
        {
            if (!mFriends[i].IsActive())
            {
                return mFriends[i];
            }
        }
        return null;

    }

    void OnGui()
    {
        Debug.Log("Yep, ongui.");
    }
}

