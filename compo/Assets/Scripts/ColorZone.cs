using UnityEngine;
using System.Collections;

public class ColorZone : MonoBehaviour
{

    private enum State
    {
        UNUSED,                 // no edges attached, not visible
        TRANSITION_TO_ZONE,     // Edges attached, not visible, edges should try to move into a good position at this point.
        IN_ZONE,                // visible, color transitioning into final color. Movement is still allowed.
        WANDERING,              // visible, edges attached, zone will transition to a random target and, once it reaches it, transition elsewhere.
        TRANSITION_TO_UNUSED,   // visible, edges attached, but trying to become invisible so that we can move to UNUSED.
        HOLDING                 // holding at position. Not sure whether this is needed.
    };

    public Edge TopEdge;
    public Edge BottomEdge;
    public Edge LeftEdge;
    public Edge RightEdge;

    public Friendly Friend;

    public bool IsEnabled
    {
        get
        {
            if (mIsEnabled == true)
            {
                if (LeftEdge == null)
                {
                    Debug.Log("VERY BAD LeftEdge is NULL");
                }

                if (RightEdge == null)
                {
                    Debug.Log("VERY BAD RightEdge is NULL");
                }

                if (TopEdge == null)
                {
                    Debug.Log("VERY BAD TopEdge is NULL");
                }
                if (BottomEdge == null)
                {
                    Debug.Log("VERY BAD BottomEdge is NULL");
                }
            }
            return this.mIsEnabled;
        }
        set
        {
            mIsEnabled = value;
            renderer.enabled = value;
            if (value)
            {
                if (BottomEdge != null)
                {
                    BottomEdge.Enabled = value;
                }
                if (TopEdge != null)
                {
                    TopEdge.Enabled = value;
                }
                if (LeftEdge != null)
                {
                    LeftEdge.Enabled = value;
                }
                if (RightEdge != null)
                {
                    RightEdge.Enabled = value;
                }

                if (LeftEdge == null)
                {
                    Debug.Log("VERY BAD LeftEdge is NULL");
                }

                if (RightEdge == null)
                {
                    Debug.Log("VERY BAD RightEdge is NULL");
                }

                if (TopEdge == null)
                {
                    Debug.Log("VERY BAD TopEdge is NULL");
                }
                if (BottomEdge == null)
                {
                    Debug.Log("VERY BAD BottomEdge is NULL");
                }
            }
            else
            {
                renderer.material.color = mTargetColor;
                if (BottomEdge != null)
                {
                    BottomEdge.RemoveFromSafezone();
                    BottomEdge = null;
                }
                if (TopEdge != null)
                {
                    TopEdge.RemoveFromSafezone();
                    TopEdge = null;
                }
                if (RightEdge != null)
                {
                    RightEdge.RemoveFromSafezone();
                    RightEdge = null;
                }
                if (LeftEdge != null)
                {
                    LeftEdge.RemoveFromSafezone();
                    LeftEdge = null;
                }
            }
        }
    }

    private bool mIsEnabled = false;
    private State mState = State.UNUSED;
    private bool mHasPlayer;
    private float mTimeWithPlayer;
    private float mTimeInState;
    private float mDesiredTimeInState;
    private float mColorTransitionTime;
    private float mDesiredColorTransitionTime = 1f;
    private Color mTargetColor;
    private Color mPreviousColor;
    private int mColorIndex;
    private Rect mTargetDimension;

    void Awake()
    {
        renderer.enabled = false;
        mTargetDimension = new Rect(0, 0, 0, 0);
    }

    // Use this for initialization
    void Start()
    {
        mHasPlayer = false;
        mTimeWithPlayer = 0f;
        //IsEnabled = false;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (!mIsEnabled)
        {
            // EARLY RETURN
            return;
        }

        switch (mState)
        {
        case State.UNUSED:
            // do nothing if we're unused.
            break;
        case State.TRANSITION_TO_ZONE:
            // We stay in this state until all Edges are in position.
            if (AllEdgesInPosition())
            {
                mState = State.IN_ZONE;
                // Reset the color transition time for when we get to processing that
                mColorTransitionTime = 0f;
            }
            break;
        case State.IN_ZONE:
            UpdateColorTransition();
            UpdatePlayerTime();
            // Update our scale and position
            FitWithinEdges();
            break;
        case State.WANDERING:
            UpdateColorTransition();
            UpdatePlayerTime();

            // See whether we've achieved our destination, and if so, then find a new destination

            // Update our scale and position
            FitWithinEdges();
            break;
        case State.TRANSITION_TO_UNUSED:
            // Do we care about player time as well?
            UpdateColorTransition();
            FitWithinEdges();
            // If we've stayed here for as long as we want, then move on to UNUSED.
            if (mDesiredTimeInState > 0 && mTimeInState > mDesiredTimeInState)
            {
                SetInternalState(State.UNUSED);
            }
            break;
        }
    }

    void UpdatePlayerTime()
    {
        // Player can be attached in this state, so update that time as well.
        if (mHasPlayer == true)
        {
            // If this safezone is one of those asshole zones that will change color on me after
            // a while, then so be it. We need to keep track how long we stay safe.
            mTimeWithPlayer += Time.deltaTime;
        }
    }

    void UpdateColorTransition()
    {
        // Deal with color transitions
        mColorTransitionTime += Time.deltaTime;
        if (mColorTransitionTime < mDesiredColorTransitionTime)
        {
            renderer.material.color = Color.Lerp(mPreviousColor, mTargetColor, mColorTransitionTime / mDesiredColorTransitionTime);
        }
        else
        {
            renderer.material.color = mTargetColor;
        }
    }

    void FitWithinEdges()
    {
        Transform thisTransform = this.transform;
     
        if (RightEdge.transform.position.x < LeftEdge.transform.position.x)
        {
            Edge swp = RightEdge;
            RightEdge = LeftEdge;
            LeftEdge = swp;
        }
     
        if (BottomEdge.transform.position.y < TopEdge.transform.position.y)
        {
            Edge swp = BottomEdge;
            BottomEdge = TopEdge;
            TopEdge = swp;
        }
     
        // This is assuming that the default scale for the SafeZoneArea is '1'. It's a pretty good assumption to make
        // AT THIS MOMENT.
        float xScale = RightEdge.transform.position.x - LeftEdge.transform.position.x - LeftEdge.transform.localScale.x / 2 - RightEdge.transform.localScale.x / 2;
        float yScale = BottomEdge.transform.position.y - TopEdge.transform.position.y - TopEdge.transform.localScale.y / 2 - BottomEdge.transform.localScale.y / 2;
     
     
        // I remember this now. thisTransform.position returns a COPY. So nothing done to it will affect the GameObject
        //Vector3 newPosition = new Vector3(SafeZoneLeftEdge.transform.position.x + SafeZoneLeftEdge.transform.localScale.x / 2 + xScale / 2, SafeZoneTopEdge.transform.position.y, thisTransform.position.z);
        Vector3 newPosition = new Vector3(
         LeftEdge.transform.position.x + (xScale + LeftEdge.transform.localScale.x) / 2,
         TopEdge.transform.position.y + (yScale + BottomEdge.transform.localScale.y) / 2,
         thisTransform.position.z);
        thisTransform.position = newPosition;
     
        Vector3 newScale = new Vector3(xScale, yScale, 1f);
        thisTransform.localScale = newScale;
    }

    bool AllEdgesInPosition()
    {
        return LeftEdge.CloseEnough && RightEdge.CloseEnough && TopEdge.CloseEnough && BottomEdge.CloseEnough;
    }

    public bool CanJumpInto()
    {
        return (mIsEnabled && AllEdgesInPosition());
    }

    public void SetWithPlayer(bool value)
    {
        mHasPlayer = value;
        mTimeWithPlayer = 0f;
    }

    public bool HasPlayer()
    {
        return mHasPlayer;
    }

    public Vector3 GetCenter()
    {

        Transform thisTransform = this.transform;

        //Debug.Log("SafeZoneArea::GetCenter() : " + thisTransform.position);

        return thisTransform.position;
        //return new Vector3(thisTransform.position.x + thisTransform.localScale.x / 2,thisTransform.position.y - thisTransform.localScale.y / 2);
    }

    public void AddEdge(Edge newEdge)
    {
        if (newEdge.Enabled)
        {

            if (newEdge.IsVertical)
            {
                if (BottomEdge == null)
                {
                    BottomEdge = newEdge;
                    newEdge.SafeZone = this;
                }
                else
                {
                    TopEdge = newEdge;
                    newEdge.SafeZone = this;
                }
            }
            else
            {
                if (LeftEdge == null)
                {
                    LeftEdge = newEdge;
                    newEdge.SafeZone = this;
                }
                else
                {
                    RightEdge = newEdge;
                    newEdge.SafeZone = this;
                }
            }
        }
        else
        {
            if (BottomEdge == null)
            {
                BottomEdge = newEdge;
                BottomEdge.IsVertical = false;
                newEdge.SafeZone = this;
            }
            else if (TopEdge == null)
            {
                TopEdge = newEdge;
                TopEdge.IsVertical = false;
                newEdge.SafeZone = this;
            }
            else if (LeftEdge == null)
            {
                LeftEdge = newEdge;
                LeftEdge.IsVertical = true;
                newEdge.SafeZone = this;
            }
            else if (RightEdge == null)
            {
                RightEdge = newEdge;
                RightEdge.IsVertical = true;
                newEdge.SafeZone = this;
            }
        }

    }

    public void SetColor(Color newColor, int index)
    {
        //Debug.Log("SafeZoneArea["+this.gameObject.name+"]::SetColor index=" +index + ", newColor=" + newColor.ToString());
        mColorIndex = index;
        mPreviousColor = this.renderer.material.color;
        mTargetColor = newColor;

        mColorTransitionTime = 0f;

    }

    public void SetTargetDimension(float x, float y, float width, float height)
    {
        //Debug.Log("SafeZoneArea::SetTargetDimension x=" + x + ", y=" + y + ", width=" + width + ", height=" + height);

        mTargetDimension.x = x;
        mTargetDimension.y = y;
        mTargetDimension.width = width;
        mTargetDimension.height = height;


        BottomEdge.SetTargetY(y + height);
        TopEdge.SetTargetY(y);

        RightEdge.SetTargetX(x + width);
        LeftEdge.SetTargetX(x);

    }

    public void SetUsed()
    {
        if (mState == State.UNUSED || mState == State.TRANSITION_TO_UNUSED)
        {
            if (IsEnabled == false)
            {
                IsEnabled = true;
                // Wait. This means that we can have no edges. This is REALLY BAD.
                if (HasNullEdges())
                {
                    throw new System.Exception("Hey, Null edges! DAMN");
                }
            }
            SetInternalState(State.TRANSITION_TO_ZONE);
        }
    }

    private void SetInternalState(State newState)
    {
        // TODO - have valid transitions/states
        mDesiredTimeInState = -1f;

        switch (newState)
        {
        case State.UNUSED:
            if (IsEnabled)
            {
                IsEnabled = false;
            }
            break;
        case State.TRANSITION_TO_UNUSED:
            mDesiredTimeInState = 2f;
            break;
        case State.TRANSITION_TO_ZONE:
        case State.HOLDING:
        case State.IN_ZONE:
        case State.WANDERING:
            if (HasNullEdges())
            {
                throw new System.Exception("At least one null edge! Check your logic");
            }
            break;
        }

        mState = newState;
        mTimeInState = 0f;
        mColorTransitionTime = 0f;
    }

    public Rect GetDimensions()
    {
        if (LeftEdge == null)
        {
            Debug.Log("LeftEdge is NULL");
        }

        if (RightEdge == null)
        {
            Debug.Log("RightEdge is NULL");
        }

        if (TopEdge == null)
        {
            Debug.Log("TopEdge is NULL");
        }
        if (BottomEdge == null)
        {
            Debug.Log("BottomEdge is NULL");
        }
        return new Rect(mTargetDimension);
    }

    public int GetSafeColorIndex()
    {
        return mColorIndex;
    }

    public float GetTimeWithPlayer()
    {
        return mTimeWithPlayer;
    }

    public bool HasNullEdges()
    {
        return LeftEdge == null || RightEdge == null || TopEdge == null || BottomEdge == null;
    }

    public void RemoveZone()
    {
        if (IsEnabled)
        {
            SetInternalState(State.TRANSITION_TO_UNUSED);
        }
    }
}
