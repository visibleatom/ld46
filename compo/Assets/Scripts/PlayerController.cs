using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour, Trackable
{

    public LevelController Level;
    public GameObject Outline;
    public GameObject ZoneIndicator;
    public AudioSource DeathSound;
    public AudioSource JumpSound;
    public AudioSource NotReady;

    private ColorZone mSafeZone;
    private ColorZone mNextTargetZone;
    private Color mTargetColor;
    private Color mCurrentColor;
    private int mTargetColorIdx;
    private float mTimeWithColor = 0f;
    private float mCamouflageTime = 1f;

    public bool mIsDead = false;
    public float mTimeAlive = 0f;
    // Use this for initialization
    void Start()
    {
        mCurrentColor = this.renderer.material.color;
        mIsDead = false;
        mTimeAlive = 0f;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (mIsDead)
        {
            ZoneIndicator.renderer.enabled = Outline.renderer.enabled = renderer.enabled = false;

            // NOTHING ELSE.
            return;
        }

        if (InputController.TeleportCommand)
        {
            JumpToSafeZone(Level.GetValidJumpZone());
        }


        if (mSafeZone != null && mSafeZone.CanJumpInto())
        {
            mTimeAlive += Time.deltaTime;

            ZoneIndicator.renderer.enabled = Outline.renderer.enabled = renderer.enabled = true;
            Vector3 newPos = mSafeZone.GetCenter();
            newPos.z = -10; //< THIS MUST MATCH the sweeper Z if we want to perform collision detection in Unity.
            transform.position = newPos;
        }
        else
        {
            ZoneIndicator.renderer.enabled = Outline.renderer.enabled = renderer.enabled = false;
        }

        if (mTimeWithColor < mCamouflageTime)
        {
            mTimeWithColor += Time.deltaTime;
            renderer.material.color = Color.Lerp(mCurrentColor, mTargetColor, mTimeWithColor / mCamouflageTime);
        }

        if (mSafeZone != mNextTargetZone && mNextTargetZone != null)
        {
            Vector3 thisPos = this.transform.position;
            Vector3 nextPos = mNextTargetZone.GetCenter();
            nextPos.z = thisPos.z;
            ZoneIndicator.transform.position = thisPos + ((nextPos - thisPos).normalized * .5f);
        }

    }

    public void SetColor(Color newColor, int colorIdx)
    {
        mTargetColorIdx = colorIdx;
        mTimeWithColor = 0f;
        mTargetColor = newColor;
        mCurrentColor = this.renderer.material.color;
    }

    public void SetTargetZone(ColorZone zone)
    {
        mNextTargetZone = zone;
    }

    public ColorZone GetCurrentZone()
    {
        return mSafeZone;
    }
    
    public ColorZone GetTargetZone()
    {
        return mNextTargetZone;
    }


    public void SetZone(ColorZone zone)
    {
        if (mSafeZone != null)
        {
            mSafeZone.SetWithPlayer(false);
        }
        mSafeZone = zone;

        zone.SetWithPlayer(true);
        ZoneIndicator.transform.position = this.transform.position;

    }

    public void JumpToSafeZone(ColorZone newSafeZone)
    {
        //Debug.Log("PlayerController::JumpToSafeZone");
        if ((mTimeWithColor >= mCamouflageTime) && newSafeZone != null && mSafeZone != newSafeZone && newSafeZone.CanJumpInto())
        {
            if (mSafeZone != null)
            {
                // Don't want to play a sound when we FIRST go into a zone
                JumpSound.Play();
            }
            SetZone(newSafeZone);

        }
        else
        {
            NotReady.Play();
        }
    }

    public bool VisibleInZone()
    {
        bool visible = false;

        // If we have no safezone attached, we aren't even rendering. We haven't even started yet, I think.
        if (mSafeZone != null)
        {
            // If we are in a safezone AND the indexes match, then we're safe.
            visible = visible || (mSafeZone.GetSafeColorIndex() != mTargetColorIdx);
            // Also, in the interest of being nice to the player, we'll assume that WHILE
            // THEY ARE SWITCHING COLORS, they are safe. So only visible after camouflage is done
            visible = visible && (mTimeWithColor >= mCamouflageTime);
        }
        return visible;
    }

    #region Trackable implementation
    public float GetPositionX()
    {
        return this.transform.position.x;
    }

    public float GetPositionY()
    {
        return this.transform.position.y;
    }

    public void Kill ()
    {
        if (!mIsDead)
        {
            mIsDead = true;
            DeathSound.Play();
        }
    }
    #endregion

    public bool IsDead()
    {
        return mIsDead;
    }

    public bool HasZone()
    {
        return mSafeZone != null;
    }
}
