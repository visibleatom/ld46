using UnityEngine;
using System.Collections;

public class InstructionController : MonoBehaviour {

    public GUIText ControlKeys;
    public GUIText ControlDescription;
    public GUIText Goal;
    public GUITexture Background;

	// Use this for initialization
	void Start () {
	    ControlKeys.material.color = Color.black;
        ControlDescription.material.color = Color.black;
        Goal.material.color = Color.black;

        ControlKeys.text = "Change color schemes (I tried to come up with good\nalternatives for colorblind folk)\n"
                           + "Pick a random target (will change some colors)\n"
                           + "Shuffle any zones that are not the player nor the\ncurrent target.\n"
                           + "Jump to the zone that matches your current color.\n"
                           + "Restart Game\n"
                           + "Pause (experimental)";

        ControlDescription.text = "1,2,3\n"
                                + "\n"
                                +"W\n"
                                +"Q\n"
                                +"\n"
                                +"Space/Click\n"
                                +"R\n"
                                +"K\n";
        Goal.text = "Goal: Survive for as long as possible\n"
                + "\n"
                + "The game ends when you are hit with a sweeper.\n"
                + "Sweeper - These will spawn at irregular intervals and\n"
                + "sweep the screen in a particular direction. They have\n"
                + "two states. Sweeping and Chasing.  They can get you\n"
                + "either way, but they only know that you don't belong\n"
                + "when you match a different zone. Jump away before\n"
                + "they catch you.";

	}

    public void DisplayTimeAlive (float mTimeAlive)
    {
        Vector3 newBgPos = Background.transform.position;
        newBgPos.y = 1000;
        Background.transform.position = newBgPos;

        ControlKeys.material.color = Color.white;
        ControlDescription.material.color = Color.white;
        Goal.material.color = Color.white;

        ControlKeys.text = "You are warrior.";

        ControlDescription.text = "";
        Goal.text = "You survived longer than most who never played.\n"
                + "\n"
                + "Time: " + mTimeAlive + " seconds\n"
                + "(press r to restart)";
    }

    public void Show (bool show)
    {
        Vector3 newPos = this.transform.position;
        newPos.y = show?0: 1000;
        this.transform.position = newPos;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
