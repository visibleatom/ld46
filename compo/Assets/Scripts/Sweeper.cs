using UnityEngine;
using System.Collections;

public interface Trackable
{
    bool VisibleInZone();

    float GetPositionX();

    float GetPositionY();

    void Kill();
};


// This line will come in from the sides of the screen at a set speed.
// If it hits the player, bad things happen.
public class Sweeper : Edge
{
    private const float APPREHENSION_DISTANCE = .3f;
    private const int MAX_TRACKED = 20;
    enum State
    {
        IDLE,
        SWEEP,
        CHASE,
    };

    Trackable mHighPriorityTarget;
    Trackable [] mLowPriorityList;

    public GameObject Outline;
    public AudioSource ChaseSource;
    public AudioSource SweepSource;

    public bool IsPaused = false;

    public float ChaseSpeedModifier = 1.1f;
    public float NoAlarmSpeedModifier = 1f;
    private State mState;
    private Vector3 mSweepDestination;
    private Rect mSweepDimensions;

    void Awake()
    {
        renderer.enabled = true;
        mState = State.IDLE;
        mHighPriorityTarget = null;
        mLowPriorityList = new Trackable[MAX_TRACKED];

    }
    // Use this for initialization
    void Start()
    {
        SetState(State.IDLE);

    }

    // Update is called once per frame
    override public void Update()
    {
        if (IsPaused)
        {
            return;
        }

        switch (mState)
        {
        case State.IDLE:

            break;
        case State.SWEEP:
            this.renderer.material.color = Color.white;
            // We should be directed towards our normal place of business
            if (GetVisibleTarget() != null)
            {
                // What the? CHASE MOTHERFUCKER, CHASE
                SetState(State.CHASE);
            }
            else if (CloseEnough)
            {
                SetState(State.IDLE);
            }
            else if (GetDistanceToTarget(mHighPriorityTarget) < APPREHENSION_DISTANCE)
            {
                // Him who we always hunt
                mHighPriorityTarget.Kill();
            }

            break;
        case State.CHASE:
            this.renderer.material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));

            Trackable highest = GetVisibleTarget();
            if (highest != null)
            {
                float distanceFromTarget = GetDistanceToTarget(highest);

                if (distanceFromTarget < APPREHENSION_DISTANCE)
                {
                    highest.Kill();
                    //SetState(State.APPREHEND);
                }
                else
                {
                    // The prey is tricky and can move.
                    SetTargetX(highest.GetPositionX());
                    SetTargetY(highest.GetPositionY());
                }
            }
            else
            {
                // Huh. Must've just been the wind.
                SetState(State.SWEEP);
            }
            break;
        }

        base.Update();

    }

    protected override float GetFakeTweener ()
    {
        return 1.0f;
    }

    private void SetState(State newState)
    {
        Debug.Log("EnemyEdge::SetState");
        mState = newState;

        Outline.renderer.enabled = renderer.enabled = (newState != State.IDLE);

        switch (newState)
        {
        case State.SWEEP:
            ChaseSource.Stop();
            SweepSource.Play();
            mCloseEnough = false;
            SetSpeedModifierX(NoAlarmSpeedModifier);
            SetSpeedModifierY(NoAlarmSpeedModifier);

            SetTargetX(mSweepDestination.x);
            SetTargetY(mSweepDestination.y);

            break;
        case State.CHASE:
            SweepSource.Stop();
            ChaseSource.Play();
            SetSpeedModifierX(ChaseSpeedModifier);
            SetSpeedModifierY(ChaseSpeedModifier);
            break;
        case State.IDLE:
            SweepSource.Stop();
            ChaseSource.Stop();
            break;

        }
    }

    public float GetDistanceToTarget (Trackable target)
    {
        float distance = 0f;
        if (IsVertical)
        {
            distance = Mathf.Abs(target.GetPositionX() - this.transform.position.x);
        }
        else
        {
            distance = Mathf.Abs(target.GetPositionY() - this.transform.position.y);
        }
        return distance;
    }
 
    public void ClearTracked()
    {
        for (int i = 0; i < MAX_TRACKED; i++)
        {
            mLowPriorityList[i] = null;
        }
    }

    public bool AddTracked(Trackable tracked, bool highPriority)
    {
        bool success = false;
        if (highPriority)
        {
            mHighPriorityTarget = tracked;
            success = true;
        }
        else
        {

            for (int i = 0; i < MAX_TRACKED; i++)
            {
                if (mLowPriorityList[i] == tracked)
                {
                    return false;
                }
            }

            for (int i = 0; i < MAX_TRACKED; i++)
            {
                if (mLowPriorityList[i] == null)
                {
                    mLowPriorityList[i] = tracked;
                    success = true;
                    break;
                }
            }
        }
        return success;
    }

    public Trackable GetVisibleTarget ()
    {
        Trackable tracked = null;

        if (mHighPriorityTarget != null && mHighPriorityTarget.VisibleInZone())
        {
            return mHighPriorityTarget;
        }

        for (int i = 0; i < MAX_TRACKED; i++)
        {
            tracked = mLowPriorityList[i];
            if (tracked != null && tracked.VisibleInZone())
            {
                return tracked;
            }
        }
        return null;
    }

    public void GetSweepPreferences(Vector3 dest, out bool leftToRight, out bool topToBottom, out bool verticalSweep)
    {

        float percentFromLeft = (dest.x - mSweepDimensions.x)/mSweepDimensions.width;
        float percentFromTop = (dest.y - mSweepDimensions.y)/mSweepDimensions.height;

        leftToRight = (percentFromLeft > .5f);
        topToBottom = (percentFromTop < .5f);

        float closestHorizontal = (percentFromLeft < .5f) ? percentFromLeft : 1 - percentFromLeft;
        float closestVertical = (percentFromTop < .5f) ? percentFromTop : 1 - percentFromTop;

        verticalSweep = (closestVertical < closestHorizontal);
        Debug.Log("EnemyEdge::GetSweepPreferences leftToRight=" + leftToRight +", topToBottom="+topToBottom +", verticalSweep="+verticalSweep);
    }

    public void BeginSweep(Vector3 origin, Vector3 dest)
    {
        Debug.Log("BeginSweep");

        // Determine whether it's a vertical or horizontal sweep
        this.IsVertical = (Mathf.Abs(origin.x - dest.x) > Mathf.Abs(origin.y - dest.y));

        this.transform.position = origin;
        mSweepDestination = new Vector3(dest.x, dest.y, dest.z);

        this.SetTargetX(dest.x);
        this.SetTargetY(dest.y);
        SetState(State.SWEEP);

    }

    public void AddDimensions (float minX, float minY, float width, float height)
    {
        mSweepDimensions = new Rect(minX, minY, width, height);
    }

    public bool IsActive()
    {
        return mState != State.IDLE;
    }
}
